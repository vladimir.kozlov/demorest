package com.vkozlov.demorest.model;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class DocumentItemDto {
    @NotEmpty
    private String name;
    @NotNull
    @Size(min=13, max=13)
    private String code;

    public DocumentItemDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
