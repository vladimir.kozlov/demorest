package com.vkozlov.demorest.model;

import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class DocumentDto {
    @NotNull
    @Size(min=9, max=9)
    private String seller;
    @NotNull
    @Size(min=9, max=9)
    private String customer;
    @NotEmpty
    @Valid
    private List<DocumentItemDto> products;

    public DocumentDto() {
    }

    public String getSeller() {
        return seller;
    }

    public void setSeller(String seller) {
        this.seller = seller;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public List<DocumentItemDto> getProducts() {
        return products;
    }

    public void setProducts(List<DocumentItemDto> products) {
        this.products = products;
    }
}
