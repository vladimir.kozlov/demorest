package com.vkozlov.demorest.controller;

import com.vkozlov.demorest.model.DocumentDto;
import com.vkozlov.demorest.service.DocumentService;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/documents")
public class DocumentController {
    private final DocumentService documentService;

    @Autowired
    public DocumentController(DocumentService documentService) {
        this.documentService = documentService;
    }

    @PostMapping
    @ResponseBody
    public ResponseEntity processForm(@Valid @RequestBody DocumentDto dto) {
        documentService.doSomething(dto.getSeller());
        return ResponseEntity.noContent().build();
    }
}
