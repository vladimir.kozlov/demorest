package com.vkozlov.demorest.service;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.function.Function;
import org.springframework.stereotype.Service;

@Service
public class DocumentService {
    private final ConcurrentHashMap<String, String> map = new ConcurrentHashMap<>();
    private ExecutorService executor
        = Executors.newSingleThreadExecutor();

    public void doSomething(String seller) {
        Function<String, String> convert = x -> x + " added to map";
        Future<String> future = compute(seller, convert);
    }

    private Future<String> compute(String k, Function<String, String> f) {
        return executor.submit(() -> map.computeIfAbsent(k, f));
    }
}
